﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApiCore.Models;

namespace WebApiCore.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TodotypesController : ControllerBase
    {
        private readonly TODOContext _context;

        public TodotypesController(TODOContext context)
        {
            _context = context;
        }

        // GET: api/Todotypes
        [HttpGet]
        public IEnumerable<Todotype> GetTodotype()
        {
            return _context.Todotype;
        }

        // GET: api/Todotypes/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetTodotype([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var todotype = await _context.Todotype.FindAsync(id);

            if (todotype == null)
            {
                return NotFound();
            }

            return Ok(todotype);
        }

        // PUT: api/Todotypes/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTodotype([FromRoute] int id, [FromBody] Todotype todotype)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != todotype.Id)
            {
                return BadRequest();
            }

            _context.Entry(todotype).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TodotypeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Todotypes
        [HttpPost]
        public async Task<IActionResult> PostTodotype([FromBody] Todotype todotype)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Todotype.Add(todotype);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (TodotypeExists(todotype.Id))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetTodotype", new { id = todotype.Id }, todotype);
        }

        // DELETE: api/Todotypes/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTodotype([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var todotype = await _context.Todotype.FindAsync(id);
            if (todotype == null)
            {
                return NotFound();
            }

            _context.Todotype.Remove(todotype);
            await _context.SaveChangesAsync();

            return Ok(todotype);
        }

        private bool TodotypeExists(int id)
        {
            return _context.Todotype.Any(e => e.Id == id);
        }
    }
}