﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace WebApiCore.Models
{
    public partial class TODOContext : DbContext
    {
        public TODOContext()
        {
        }

        public TODOContext(DbContextOptions<TODOContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Todo> Todo { get; set; }
        public virtual DbSet<Todotype> Todotype { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Server=AKPC;Database=TODO;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Todo>(entity =>
            {
                entity.ToTable("TODO");

                entity.Property(e => e.Title).HasMaxLength(50);

                entity.Property(e => e.TodoDate).HasColumnType("date");

                entity.HasOne(d => d.Type)
                    .WithMany(p => p.Todo)
                    .HasForeignKey(d => d.TypeId)
                    .HasConstraintName("FK_TODO_TODOType");
            });

            modelBuilder.Entity<Todotype>(entity =>
            {
                entity.ToTable("TODOType");

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasMaxLength(50);
            });
        }
    }
}
