﻿using System;
using System.Collections.Generic;

namespace WebApiCore.Models
{
    public partial class Todotype
    {
        public Todotype()
        {
            Todo = new HashSet<Todo>();
        }

        public int Id { get; set; }
        public string Type { get; set; }

        public ICollection<Todo> Todo { get; set; }
    }
}
