﻿using System;
using System.Collections.Generic;

namespace WebApiCore.Models
{
    public partial class Todo
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int? TypeId { get; set; }
        public DateTime? TodoDate { get; set; }

        public Todotype Type { get; set; }
    }
}
