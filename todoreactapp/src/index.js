import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import React, { Component } from 'react';
import { Layout } from './components/Layout/Layout';
import { Home } from './Pages/Home/Home';
import { EditTodos } from './Pages/EditTodos/EditTodos.js';
import { EditTodoTypes } from './Pages/EditTodoTypes/EditTodoTypes.js';

class App extends Component {
    render() {
        return (
            <Router>
                <Layout>
                    <Route exact path='/' component={Home} />
                    <Route path='/edittodos' component={EditTodos} />
                    <Route path='/edittodotypes' component={EditTodoTypes} />
                </Layout>
            </Router>
        );
    }
}

ReactDOM.render(<App />, document.getElementById('root'));
