const apiUrl = "http://localhost:52754/api";

// Todos
export function getTodoDetails() {
    return fetch(`${apiUrl}/GetTodoDetails`);
}

export function deleteTodo(id) {
    return fetch(`${apiUrl}/Todos/${id}`, {
        method: 'delete',
    });
}

export function newTodo(todo) {
    return fetch(`${apiUrl}/Todos/`, {
        method: "post",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(todo),
    });

}

// Todo Types
export function getTodoTypes() {
    return fetch(`${apiUrl}/Todotypes`);
}

export function deleteTodoType(id) {
    return fetch(`${apiUrl}/Todotypes/${id}`, {
        method: 'delete',
    });
}

export function newTodoType(type) {
    return fetch(`${apiUrl}/Todotypes/`, {
        method: "post",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(type),
    });
}