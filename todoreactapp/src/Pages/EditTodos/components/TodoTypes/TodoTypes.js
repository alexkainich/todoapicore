import React, { Component } from 'react';
import { getTodoTypes } from 'utils/ApiFunctions.js';

export class TodoTypes extends Component {

    constructor(props) {
        super(props);
        this.state = { types: [] };
    }

    componentDidMount()
    {
        getTodoTypes()
            .then(response => response.json())
            .then(data => {
                this.setState((prevState) => ({ types: data }));
            });
        }

    render() {
        return (
            <div>
                <select className='newTodoType' onChange={this.props.callBack}>
                    {(this.props.selection === "") ? <option value="" selected></option> : <option value=""></option>}
                    {this.state.types.map(type => <option key={type.id} value={type.id}>{type.type}</option>)}
                </select>
            </div>
        );
    }
}
