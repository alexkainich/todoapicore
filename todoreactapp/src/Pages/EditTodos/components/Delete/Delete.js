import React, { Component } from 'react';
import "./Delete.css";
import { deleteTodo } from 'utils/ApiFunctions.js'

export class Delete extends Component {

    render() {
        return (
            <div>
                <button className="DeleteButton" onClick={() => deleteTodo(this.props.id).then(this.props.callBack)}>X</button>
            </div>
        );
    }
}
