import React, { Component } from 'react';
import "./New.css";
import { newTodo } from 'utils/ApiFunctions.js'

export class New extends Component {

    constructor(props) {
        super(props);
        this.state = { error: null };
    }

    validateTodo = () => {
        newTodo(this.props.item).then(this.props.callBack);
    }

    render() {
        return (
            <div>
                <button className="NewButton" onClick={this.validateTodo}>Add</button>
            </div>
        );
    }
}
