import React, { Component } from 'react';
import { Delete } from './components/Delete/Delete.js';
import "./EditTodos.css";
import { getTodoDetails } from 'utils/ApiFunctions.js'
import { New } from './components/New/New.js'
import { TodoTypes } from './components/TodoTypes/TodoTypes.js'

const emptyNew = {
    "title": "",
    "description": "",
    "typeId": "",
    "todoDate": "",
}

export class EditTodos extends Component {

    constructor(props) {
        super(props);
        this.state = { todos: [], loading: true, new: emptyNew };
    }

    componentDidMount() {
        this.refreshTodoTable();
    }

    refreshTodoTable = () => {
        getTodoDetails()
            .then(response => response.json())
            .then(data => {
                this.setState((prevState) => ({ todos: data, loading: false, new: prevState.new }));
            });
    }

    constructDateString = (input) => {
        let dateObject = new Date(input);

        return `${dateObject.getDate()}-${dateObject.getMonth() + 1}-${dateObject.getFullYear()}`;
    }


    updateNewTodo = (e) => {
        let newProperty = e.target.className;
        let newValue = e.target.value;

        this.setState((prevState) => ({
            todos: prevState.todos,
            loading: prevState.loading,
            new: {
                "title": (newProperty === "newTodoTitle") ? newValue : prevState.new.title,
                "description": (newProperty === "newTodoDesc") ? newValue : prevState.new.description,
                "typeId": (newProperty === "newTodoType") ? newValue : prevState.new.typeId,
                "todoDate": (newProperty === "newTodoDate") ? newValue : prevState.new.todoDate,
            },
        }));
    }

    afterAddNew = () => {
        this.clearNewTodo();
        this.refreshTodoTable();
    }

    clearNewTodo = () => {
        this.setState((prevState) => ({
            todos: prevState.todos,
            loading: prevState.loading,
            new: {
                "title": "",
                "description": "",
                "typeId": "",
                "todoDate": "",
            },
        }));
    }

    renderTodosTable = (todos) => {
        return (
            <table className='todoTable'>
                <thead>
                    <tr>
                        <th>Title</th>
                        <th>Date</th>
                        <th>Description</th>
                        <th>Type</th>
                    </tr>
                </thead>
                <tbody>
                    {todos.map(todo =>
                        <tr key={todo.id}>
                            <td>{todo.title}</td>
                            <td>{this.constructDateString(todo.todoDate)}</td>
                            <td>{todo.description}</td>
                            <td>{todo.type ? todo.type.type : ""}</td>
                            <td><Delete id={todo.id} callBack={this.refreshTodoTable} /></td>
                        </tr>
                    )}
                    <tr className='newTodo'>
                        <td><textarea className='newTodoTitle' type="text" onChange={this.updateNewTodo} value={this.state.new.title} /></td>
                        <td><input className='newTodoDate' type="date" onChange={this.updateNewTodo} value={this.state.new.todoDate} /></td>
                        <td><textarea className='newTodoDesc' type="text" onChange={this.updateNewTodo} value={this.state.new.description} /></td>
                        <td><TodoTypes selection={this.state.new.typeId} callBack={this.updateNewTodo} /></td>
                        <td><New item={this.state.new} callBack={this.afterAddNew} /></td>
                    </tr>
                </tbody>
            </table>
        );
    }

    render() {
        let contents = this.state.loading
            ? <p><em>Loading...</em></p>
            : this.renderTodosTable(this.state.todos);

        return (
            <div>
                <h1>Todo List</h1>
                {contents}
            </div>
        );
    }
}
