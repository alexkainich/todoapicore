import React, { Component } from 'react';

export class Home extends Component {

    render() {
        return (
            <div>
                <h1>Welcome to your TODO list !</h1>
            </div>
        );
    }
}
