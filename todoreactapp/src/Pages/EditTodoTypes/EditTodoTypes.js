import React, { Component } from 'react';
import { Delete } from './components/Delete/Delete.js';
import { getTodoTypes } from 'utils/ApiFunctions.js'
import "./EditTodoTypes.css";
import { New } from "./components/New/New.js";

export class EditTodoTypes extends Component {

    constructor(props) {
        super(props);
        this.state = { types: [], new: "" };
    }

    componentDidMount() {
        this.refreshTodoTypesTable();
    }

    refreshTodoTypesTable = () => {
        getTodoTypes()
            .then(response => response.json())
            .then(data => {
                this.setState((prevState) => ({ types: data, new: prevState.new }));
            });
    }

    renderTodoTypesTable = (types) => {
        return (
            <table className='todoTypesTable'>
                <thead>
                    <tr>
                        <th>Event</th>
                    </tr>
                </thead>
                <tbody>
                    {types.map(type =>
                        <tr key={type.id}>
                            <td>{type.type}</td>
                            <td><Delete id={type.id} callBack={this.refreshTodoTypesTable}/></td>
                        </tr>
                    )}
                    <tr className='newType'>
                        <td><textarea type="text" onChange={this.updateNewType} value={this.state.new} /></td>
                        <td><New item={this.getNewTypeJson()} callBack={this.afterAddNew} /></td>
                    </tr>
                </tbody>
            </table>
        );
    }

    getNewTypeJson = () => {
        let result = {
            type: this.state.new
        }

        return result;
    }

    afterAddNew = () => {
        this.clearNewType();
        this.refreshTodoTypesTable();
    }

    clearNewType = () => { 
        this.setState((prevState) => ({ types: prevState.types, new: "" }));  
    }

    updateNewType = (e) => {
        let newType = e.target.value;

        this.setState((prevState) => ({ types: prevState.types, new: newType }));         
    }

    render() {
        let contents = this.state.loading
            ? <p><em>Loading...</em></p>
            : this.renderTodoTypesTable(this.state.types);

        return (
            <div>
                <h1>Todo List</h1>
                {contents}
            </div>
        );
    }
}
