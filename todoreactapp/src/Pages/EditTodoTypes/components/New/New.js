import React, { Component } from 'react';
import "./New.css";
import { newTodoType } from 'utils/ApiFunctions.js'

export class New extends Component {

    validateTodoType = () => {        
        newTodoType(this.props.item).then(this.props.callBack);
    }

    render() {
        return (
            <div>
                <button className="NewButton" onClick={this.validateTodoType}>Add</button>
            </div>
        );
    }
}
