import React, { Component } from 'react';
import "./Delete.css";
import { deleteTodoType } from 'utils/ApiFunctions.js'

export class Delete extends Component {

    constructor(props) {
        super(props);
        this.state = { error: null };
    }

    onError = (error) => {       
        this.setState({ error: error });
    }

    clicked = () => {
        this.setState({ error: null });

        deleteTodoType(this.props.id).then(this.props.callBack).catch(this.onError);
    }

    render() {
        return (
            <div>
                <button className="DeleteButton" onClick={this.clicked}>X</button>
                {this.state.error ? <div>The type is used by one or more todos</div> : null}
            </div>
        );
    }
}
