import React, { Component } from 'react';
import './NavMenu.css';
import { Link } from 'react-router-dom';

export class NavMenu extends Component {

    render() {
        return (
            <nav>
                <Link to={'/'}>Home</Link>
                <br />
                <Link to={'/edittodos'}>Edit Todos</Link>
                <br />
                <Link to={'/edittodotypes'}>Edit Todo Types</Link>
            </nav>
        );
    }
}
